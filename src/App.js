import React from "react";
import { Tree } from "./Tree";

const db = [
    {
        id: 1,
        parentId: null,
        value: 'Element1',
        childs: [
            {
                id: 2,
                parentId: 1,
                deleted: true,
                value: 'Element2',
                childs: [
                    {
                        id: 3,
                        parentId: 2,
                        deleted: true,
                        value: 'Element3',
                        childs: []
                    }
                ]
            },
            {
                id: 4,
                parentId: 1,
                value: 'Element4',
                childs: [
                    {
                        id: 5,
                        parentId: 4,
                        value: 'Element5',
                        childs: [
                            {
                                id: 6,
                                parentId: 5,
                                value: 'Element6',
                                childs: []
                            },
                        ]
                    },
                    {
                        id: 7,
                        parentId: 4,
                        value: 'Element7',
                        childs: []
                    }
                ]
            }
        ]
    }
];
export function App(props){

    const [cachedNextId, setCachedNextId] = React.useState(10);
    const [edit, setEdit] = React.useState(false);
    const [cachedActive, setCachedActive] = React.useState(null);
    const [cachedTree, setCachedTree] = React.useState([]);
    const [dbActive, setDbActive] = React.useState(null);
    const [dbTree, setDbTree] = React.useState(db);

    function changeValue(arr, id, value){
        if (arr.length === 0){
            return arr;
        }           
        else {
            return arr.map((element) => {
                if (id === element.id){
                    return {...element, value}
                }
                else {
                    return {...element, childs: changeValue(element.childs, id, value)}
                }
            })
        }
    }
    function addNewElement(arr, id, nextId){
        if(arr.length === 0){
            return arr;
        } else {
            return arr.map((element) => {
                if(id === element.id){
                    return{...element, childs: [...element.childs, {id: nextId, parentId: id, value: `NewElement${nextId}`, childs: []}]}
                }
                else{
                    return {...element, childs: addNewElement(element.childs, id)}
                }
            })
        }
    }

    function findById(arr, id){
        if(arr.length === 0){
            return null;
        } else {
            for (let item of arr ){
                if(item.id === id){
                    return item;
                }
                const founded = findById(item.childs, id)
                if(founded)
                    return founded;
            }
            return null;
        }
    }

    function extractChildsAndMe(arr, id){
        const result = [];
        const childs = [];
        let me = null;

        function trace(arr, result){
            for (let item of arr){
                if (item.id === id){
                    me = item;
                    childs.push(...item.childs);
                    continue;
                }
                if(item.parentId === id){
                    childs.push(item);
                    continue;
                }
                const newItem = {...item, childs: []}
                result.push(newItem);
                trace(item.childs, newItem.childs);
            }
        }

        trace(arr, result);
        return {result, childs, me}

    }

    function insertElement(arr, item){
        if(item.parentId === null){
            return [...arr, item];
        }
        return arr.map((element) => {
            if(item.parentId === element.id){
                return{...element, childs: [...element.childs, item]}
            }
            else{
                return {...element, childs: insertElement(element.childs, item)}
            }
        })
    }

    function downloadElement(arr, item){       
       const {result, childs, me} = extractChildsAndMe(arr, item.id);
       console.log(item.parentId, findById(result, item.parentId), result)
        if (item.parentId === null || findById(result, item.parentId) !== null){
            console.log('1')
            return insertElement(result, {...me, ...item, childs: childs})
        } else{
            console.log('2')
            return insertElement(result, {
                id: item.parentId,
                parentId: null,
                value: `???${item.parentId}`,
                childs: [{...item, childs: childs}]
            })
        }
    }

    function deleteChilds(arr){
        if (arr.length === 0)
            return arr;
        else {
            return arr.map((element) => {
                return {...element, deleted: true, childs: deleteChilds(element.childs)}
            })
        }
    }

    function deleteElement(arr, id){
        if(arr.length === 0){
            return arr;
        } else {
            return arr.map((element) => {
                if (id === element.id){
                    return {...element, deleted: true, childs: deleteChilds(element.childs)}
                }
                else {
                    return {...element, childs: deleteElement(element.childs, id)}
                }
            })
        }
    }

    return (
        <div className = 'form'>
            <div className = 'viewWrapper'>
                <div className = 'treeView'>
                    <Tree tree = {cachedTree} 
                        edit = {edit} 
                        onChangeValue = {(id, value) => {setCachedTree(changeValue(cachedTree, id, value))}}
                        active = {cachedActive}
                        setActive = {setCachedActive}
                    />
                </div>
                <button className = 'getElementButton' 
                    onClick = {() => {
                        const item = findById(dbTree, dbActive);
                        setCachedTree(downloadElement(cachedTree, {
                            id: item.id,
                            parentId: item.parentId,
                            value: item.value,
                            deleted: item.deleted
                        }))
                        console.log(downloadElement(cachedTree, {
                            id: item.id,
                            parentId: item.parentId,
                            value: item.value,
                            deleted: item.deleted
                        }));
                    }
                }
                >download</button>
                <div className = 'treeView'>
                    <Tree tree = {dbTree} 
                          active = {dbActive} 
                          setActive = {setDbActive} 
                />
                </div>
            </div>
            <div className = 'buttonPanel'>
                <button className = 'createElementButton' 
                        onClick = {
                            () => {
                                if(cachedActive !== null){
                                    setCachedTree(addNewElement(cachedTree, cachedActive, cachedNextId));
                                    setCachedNextId(cachedNextId + 1);
                                } else{
                                    if(cachedTree.length === 0){
                                        setCachedTree([{ id: cachedNextId, value: `NewElement${cachedNextId}`, childs:[]}]);
                                        setCachedNextId(cachedNextId + 1);
                                    }
                                }
                            }
                        }
                >+</button>
                <button className = 'deleteElementButton'
                        onClick = {() => {
                            if (cachedActive !== null){
                                setCachedTree(deleteElement(cachedTree, cachedActive));
                                setCachedActive(null);
                            }
                        }}        
                >-</button>
                <button className = {edit ? 'editElementButton active' : 'editElementButton'} onClick = {() => edit ? setEdit(false) : setEdit(true)}>a</button>
                <button className = 'applyEvents'>Apply</button>
                <button className = 'resetEvents' 
                        onClick = {() => {
                            setCachedTree([]);
                            setDbTree(db);
                        }}
                >Reset</button>
            </div>
        </div>
        )

}
// default export CachedTreeView;