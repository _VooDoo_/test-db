import React from "react";

export function TreeView(props){
    const [active, setActive] = React.useState(null);

    return <Tree tree = {props.tree} 
                active = {active} 
                setActive = {setActive} 
                edit = {props.edit}  
                onChangeValue = {props.onChangeValue}
            />
    
}

export function Tree(props){
    if (props.tree.length === 0)
        return null;
    else {
        return props.tree.map((element, index) => {
            function renderElement(){
                if(element.deleted === true){
                    return <div className = 'value deleted'>{element.value}</div>
                }
                if(props.edit && props.active === element.id){
                    return <input className = 'editInput' value = {element.value}
                                onChange = {(e) => {
                                    props.onChangeValue(element.id, e.target.value);
                                }}
                            ></input>
                }
                return <div className = { 
                            props.active === element.id ? 'value active' : 'value'} 
                            onClick = {() => {props.setActive(element.id)}}
                        >{element.value}</div>
            }
            return (
                <div className = 'parent' key = {element.id}>
                    {renderElement()}
                    <div className = 'childs'>
                        <Tree tree = {element.childs} 
                              active = {props.active} 
                              setActive = {props.setActive} 
                              edit = {props.edit} 
                              onChangeValue = {props.onChangeValue}
                        />
                    </div>
                </div>
            )
        })
    }
}

